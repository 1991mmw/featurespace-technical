package org.example.service;

import org.example.model.Customer;
import org.example.model.Merchant;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class EventProcessorTest {
    private static EventProcessor eventProcessor;

    @BeforeAll
    static void setUp() {
        eventProcessor = new EventProcessor();
        eventProcessor.readFile();
        ObjectGenerator objectGenerator = new ObjectGenerator();
        objectGenerator.init();
    }

    @Test
    void shouldReturn_top5CustomersWithHighestAverageTransactionAmount() {
        //given
        eventProcessor.readFile();
        //when
        List<Customer> customers = eventProcessor.top5CustomersWithHighestAverageTransactionAmount();
        //then
        assertThat(customers).isNotNull();
        assertThat(customers.get(0)).isInstanceOf(Customer.class);
        assertThat(customers.size()).isEqualTo(5);
    }

    @Test
    void shouldReturn_top5MerchantsWithHighestAverageTransactionAmount() {
        //given
        eventProcessor.readFile();
        //when
        List<Merchant> merchantList = eventProcessor.top5MerchantsWithHighestAverageTransactionAmount();
        //then
        assertThat(merchantList).isNotNull();
        assertThat(merchantList.get(0)).isInstanceOf(Merchant.class);
        assertThat(merchantList.size()).isEqualTo(5);
    }

    @Test
    void shouldReturn_top5CustomersWithHighestRemainingBalance() {
        //given
        eventProcessor.readFile();
        //when
        List<Customer> customers = eventProcessor.top5CustomersWithHighestRemainingBalance();
        //then
        assertThat(customers).isNotNull();
        assertThat(customers.get(0)).isInstanceOf(Customer.class);
        assertThat(customers.size()).isEqualTo(5);
    }
}
