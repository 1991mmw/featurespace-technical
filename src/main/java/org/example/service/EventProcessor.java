package org.example.service;

import com.google.gson.Gson;
import org.example.model.Customer;
import org.example.model.Merchant;
import org.example.model.Transaction;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.averagingDouble;
import static java.util.stream.Collectors.groupingBy;
import static org.example.service.ObjectGenerator.customers;
import static org.example.service.ObjectGenerator.merchants;

public class EventProcessor {
    private final Gson gson = new Gson();
    private List<Transaction> transactions;

    public void readFile() {
        Path path = Paths.get("src/main/resources/generated/events.txt");
        try {
            transactions = Files.readAllLines(path).stream()
                    .filter(keyword -> keyword.contains("TRANSACTION"))
                    .map(transaction -> gson.fromJson(transaction, Transaction.class))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Customer> top5CustomersWithHighestAverageTransactionAmount() {
        List<Customer> customerList = new ArrayList<>();
        Map<String, Double> averageTransactions = groupByAverageTransactions("customer");
        averageTransactions.forEach((key, value) -> System.out.println("customerId=" + key + ", averageTransactionAmount=" + value));
        aggregateCustomers(customerList, averageTransactions);
        return customerList;
    }

    public List<Merchant> top5MerchantsWithHighestAverageTransactionAmount() {
        List<Merchant> merchantList = new ArrayList<>();
        Map<String, Double> averageTransactions = groupByAverageTransactions("merchant");
        averageTransactions.forEach((key, value) -> System.out.println("merchantId=" + key + ", averageTransactionAmount=" + value));
        aggregateMerchants(merchantList, averageTransactions);
        return merchantList;
    }

    public List<Customer> top5CustomersWithHighestRemainingBalance() {
        return customers.stream()
                .sorted(Comparator.comparingDouble((Customer x) -> x.getBalance().doubleValue()).reversed())
                .limit(5)
                .collect(Collectors.toList());
    }

    private void aggregateMerchants(List<Merchant> merchantList, Map<String, Double> averageTransactions) {
        for (Merchant merchant : merchants) {
            averageTransactions.forEach((key, value) -> {
                if (key.equals(merchant.getMerchantId())) {
                    merchantList.add(merchant);
                }
            });
        }
    }

    private void aggregateCustomers(List<Customer> customerList, Map<String, Double> averageTransactions) {
        for (Customer customer : customers) {
            averageTransactions.forEach((key, value) -> {
                if (key.equals(customer.getCustomerId())) {
                    customerList.add(customer);
                }
            });
        }
    }

    private Map<String, Double> groupByAverageTransactions(String user) {
        if (user.equals("customer")) {
            return groupCustomers();
        } else {
            return groupMerchants();
        }
    }

    private Map<String, Double> groupMerchants() {
        return transactions.stream()
                .collect(groupingBy(Transaction::getMerchantId,
                        averagingDouble(x -> x.getAmount().doubleValue())))
                .entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .limit(5)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private Map<String, Double> groupCustomers() {
        return transactions.stream()
                .collect(groupingBy(Transaction::getCustomerId,
                        averagingDouble(x -> x.getAmount().doubleValue())))
                .entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .limit(5)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }
}
