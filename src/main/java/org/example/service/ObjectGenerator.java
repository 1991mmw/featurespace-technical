package org.example.service;

import com.google.gson.Gson;
import com.google.gson.internal.bind.util.ISO8601Utils;
import org.example.exception.DataEntryException;
import org.example.model.Customer;
import org.example.model.Deposit;
import org.example.model.EventType;
import org.example.model.Merchant;
import org.example.model.Transaction;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import static java.util.Date.from;

public class ObjectGenerator {
    private static final String EVENTS_TXT = "src/main/resources/generated/events.txt";
    protected static final List<Customer> customers = new ArrayList<>(Arrays.asList(
            new Customer(), new Customer(), new Customer(), new Customer(), new Customer(),
            new Customer(), new Customer(), new Customer(), new Customer(), new Customer()
    ));
    protected static final List<Merchant> merchants = new ArrayList<>(Arrays.asList(
            new Merchant(), new Merchant(), new Merchant(), new Merchant(), new Merchant(),
            new Merchant(), new Merchant(), new Merchant(), new Merchant(), new Merchant()
    ));
    private final Gson gson = new Gson();

    public void init() {
        cleanEvents();
        generateDepositEvent();
        generateTransactionEvent();
    }

    private void cleanEvents() {
        try {
            new PrintWriter(EVENTS_TXT).close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void generateDepositEvent() {
        for (Customer customer : customers) {
            BigDecimal depositValue = BigDecimal.valueOf(ThreadLocalRandom.current().nextInt(100, 1000));
            appendToFile(gson.toJson(buildDepositEvent(customer, depositValue)));
            customer.setBalance(depositValue);
        }
    }

    private Deposit buildDepositEvent(Customer customer, BigDecimal depositValue) {
        return new Deposit(
                EventType.DEPOSIT,
                UUID.randomUUID().toString(),
                customer.getCustomerId(),
                ISO8601Utils.format(from(Instant.now())),
                depositValue);
    }

    private void generateTransactionEvent() {
        for (Merchant merchant : merchants) {
            for (Customer customer : customers) {
                BigDecimal randomAmount = BigDecimal.valueOf(ThreadLocalRandom.current().nextInt(5, 10));
                int randomDay = ThreadLocalRandom.current().nextInt(0, 20);
                appendToFile(gson.toJson(buildTransactionEvent(merchant, customer, randomAmount, randomDay)));
                customer.setBalance(customer.getBalance().subtract(randomAmount));
                merchant.setBalance(merchant.getBalance().add(randomAmount));
            }
        }
    }

    private Transaction buildTransactionEvent(Merchant merchant, Customer customer, BigDecimal randomAmount,
            int randomDay) {
        return new Transaction(
                EventType.TRANSACTION,
                UUID.randomUUID().toString(),
                customer.getCustomerId(),
                merchant.getMerchantId(),
                ISO8601Utils.format(from(Instant.now().plus(randomDay, ChronoUnit.DAYS))),
                randomAmount
        );
    }

    private void appendToFile(String json) {
        try {
            final Path path = Paths.get(EVENTS_TXT);
            Files.write(path, Collections.singletonList(json), StandardCharsets.UTF_8, StandardOpenOption.APPEND);
        } catch (final IOException ioe) {
            throw new DataEntryException(ioe.getMessage());
        }
    }
}
