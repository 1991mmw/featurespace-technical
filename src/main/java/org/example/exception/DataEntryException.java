package org.example.exception;

public class DataEntryException extends RuntimeException {
    public DataEntryException(String message) {
        super(message);
    }
}
