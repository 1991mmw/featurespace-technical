package org.example;

import org.example.model.Customer;
import org.example.model.Merchant;
import org.example.service.EventProcessor;
import org.example.service.ObjectGenerator;

public class Application {
    public static void main(String[] args) {
        initiateObjects();
        EventProcessor eventProcessor = new EventProcessor();
        eventProcessor.readFile();
        System.out.println("The 5 customers with the highest average transaction amount");
        eventProcessor.top5CustomersWithHighestAverageTransactionAmount().stream().map(Customer::toString)
                .forEach(System.out::println);
        System.out.println("The 5 merchants with the highest average transaction amount");
        eventProcessor.top5MerchantsWithHighestAverageTransactionAmount().stream().map(Merchant::toString)
                .forEach(System.out::println);
        System.out.println("The 5 customers with the greatest remaining balance in their account");
        eventProcessor.top5CustomersWithHighestRemainingBalance().stream().map(Customer::toString)
                .forEach(System.out::println);

    }

    private static void initiateObjects() {
        ObjectGenerator objectGenerator = new ObjectGenerator();
        objectGenerator.init();
    }
}
