package org.example.model;

import java.math.BigDecimal;
import java.util.UUID;

public class Merchant {
    private final String merchantId;
    private BigDecimal balance;

    public Merchant() {
        this.merchantId = UUID.randomUUID().toString();
        this.balance = BigDecimal.ZERO;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Merchant{" +
                "merchantId='" + merchantId + '\'' +
                ", balance=" + balance +
                '}';
    }
}
