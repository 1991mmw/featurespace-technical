package org.example.model;

import java.math.BigDecimal;

public class Deposit {
    private EventType eventType;
    private String depositId;
    private String customerId;
    private String time;
    private BigDecimal amount;

    public Deposit() {
    }

    public Deposit(EventType eventType, String depositId, String customerId, String time, BigDecimal amount) {
        this.eventType = eventType;
        this.depositId = depositId;
        this.customerId = customerId;
        this.time = time;
        this.amount = amount;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public String getDepositId() {
        return depositId;
    }

    public void setDepositId(String depositId) {
        this.depositId = depositId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
