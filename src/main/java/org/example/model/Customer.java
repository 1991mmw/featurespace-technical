package org.example.model;

import java.math.BigDecimal;
import java.util.UUID;

public class Customer {
    private final String customerId;
    private BigDecimal balance;

    public Customer() {
        this.customerId = UUID.randomUUID().toString();
        this.balance = BigDecimal.ZERO;
    }

    public String getCustomerId() {
        return customerId;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "customerId='" + customerId + '\'' +
                ", balance=" + balance +
                '}';
    }
}
