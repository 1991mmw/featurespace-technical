package org.example.model;

import java.math.BigDecimal;

public class Transaction {
    private final EventType eventType;
    private final String transactionId;
    private final String customerId;
    private final String merchantId;
    private final String time;
    private final BigDecimal amount;

    public Transaction(EventType eventType, String transactionId, String customerId, String merchantId,
            String time, BigDecimal amount) {
        this.eventType = eventType;
        this.transactionId = transactionId;
        this.customerId = customerId;
        this.merchantId = merchantId;
        this.time = time;
        this.amount = amount;
    }

    public String getCustomerId() {
        return customerId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "eventType=" + eventType +
                ", transactionId='" + transactionId + '\'' +
                ", customerId='" + customerId + '\'' +
                ", merchantId='" + merchantId + '\'' +
                ", time='" + time + '\'' +
                ", amount=" + amount +
                '}';
    }
}
