package org.example.model;

public enum EventType {
    TRANSACTION, DEPOSIT
}
