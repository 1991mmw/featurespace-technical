# Technical Test

The project "technical test" is an application for generating objects and extracting required data

## Installation

This is a simple maven project with all libraries added, no special tools are required.

## Usage

Application is started from main in Application.java and prints 3 sets of data

- The 5 customers with the highest average transaction amount
- The 5 merchants with the highest average transaction amount
- The 5 customers with the greatest remaining balance in their account

## Future Implementations

- The 5 merchants with the shortest time between seeing event N for that merchant
 and seeing event N+4 for the same merchant
